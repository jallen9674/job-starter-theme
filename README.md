# Genesis Starter Theme Scaffolding

## Demo URL

* WPEngine: (In iloveseo account) - http://hcgenesistheme.wpengine.com/
* HTTP User: hcgenesistheme
* HTTP Pass: hennessey

## Local Setup

* Site should be running locally at `https://[site].local`
* In `gulpfile.js` `productURL` and and `projectURL` should be updated according to the current site
* `npm` and `gulp` should be installed
* Run `npm install`

## Local Development

* NOTE: This child theme requires the base genesis framework theme
* Run `gulp`
* All `scss` changes will be auto refreshed
* All `js` changes will be auto refreshed
* Local Server will be at `https://localhost:3000/`

## Note: CSS Change Information

* For small css changes, please edit the `./css/standalone-styles.css` file. That file can be edited directly. The `./css/scss-styles.css` file is compiled from the `./scss/` files and should not be edited directly.

## Updates and Issues

* Please checkout the issue tracker.