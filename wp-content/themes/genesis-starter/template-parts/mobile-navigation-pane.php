<?php 


/*--------------------------------
This is the mobile navigation pane
---------------------------------*/

function hennessey_mobile_nav_pane(){
    ?>

    <div class="mobile-navigation-pane" id="mobile-menu">

        <div class="mobile-navigation-pane__inner">
        
            <div class="mobile-navigation-pane__header">
                <span>Navigation</span>
                <i class="fa fa-close navigation-pane-close"></i>
            </div>

            <div class="mobile-navigation-pane__navigation">

                <nav role="navigation">
                    <?php 
                        wp_nav_menu(
                            array(
                                'container' => false,                           
                                'container_class' => 'menu cf',                 
                                'menu' => 'All Pages',  
                                'menu_class' => 'mobile-nav',               
                                'theme_location' => 'main-nav',                 
                                'before' => '',                                 
                                'after' => '',                                  
                                'link_before' => '',                            
                                'link_after' => '',                             
                                'depth' => 0,                                   
                                'fallback_cb' => ''                             
                            )
                        ); 
                    ?>               
                </nav>

            </div>
        
        </div>

    </div>
    

    <?php 
}


/*--------------------------------
Mobile Header
---------------------------------*/

function hennessey_mobile_nav_header(){
 ?>

    <div class="site-header__inner mobile-header">

        <div class="notification-bar">
            <div class="notification-bar__inner">
                <span class="notification-bar__left">
                    Hablamos Español.
                </span>
                <span class="notification-bar__right">
                    Call Us 24/7/365
                </span>
            </div>
        </div>
                    
        <div class="mobile-header__logo">
            <a href="<?php echo site_url(); ?>">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-full.svg" alt="Logo" class="mobile-header__logo--default">
            </a>
        </div>

        <div class="mobile-header__phone">
            <a href="tel:<?php echo hennessey_phone_display(); ?>"><?php echo hennessey_phone_display(); ?></a>
        </div>

        <div class="mobile-header__navigation-toggle navigation-pane-toggle">
            
            <span>Menu</span>
            <span><i class="fa fa-bars"></i></span>

        </div>

    </div>

 <?php 
}