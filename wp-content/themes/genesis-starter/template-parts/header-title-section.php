<?php 
/*---------------------------------------
Main H1 Section on General Interior Pages
----------------------------------------*/

function hennessey_single_interior_header(){
    ?>

    <?php if (  is_singular() && !is_front_page() ) { ?>

        <div class="interior-header">
            <div class="interior-header__top">
                <h1 class="page-title interior-header__title"><?php the_title(); ?></h1>
            </div>

            <div class="interior-header__breadcrumbs">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                ?>
           </div>
        </div>

    <?php } else if ( is_post_type_archive('hc_glossary') ){ 
    
    //Glossary archive title
    ?>

        <div class="interior-header">
            <div class="interior-header__top">
                <h1 class="page-title interior-header__title">Legal Glossary</h1>            
            </div>
        </div>

    <?php } else { ?>

        <?php if (!is_front_page()) { genesis_do_post_title(); } ?>

    <?php } //End Else?>

    <?php 
}


function hennessey_archive_header(){
    ?>

    <div class="interior-header">
        <div class="interior-header__top">
            <h1 class="page-title interior-header__title">
            
            <?php 
            
              if (is_category()) { 
                echo 'Posts Categorized: '; single_cat_title(); 
              } elseif (is_tag()) { 
                echo 'Posts Tagged: ';   single_tag_title(); 
              } else {
                the_archive_title();
              } 
              
              if ( is_paged() ){
                  $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                  echo '- Page ' . $currentPageNum;
              }
              
            ?>
            </h1>  
               
        </div>
        <div class="interior-header__breadcrumbs">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                ?>
           </div>     
    </div>
    
    <?php
}

function hennessey_blog_header() {
    ?>
  
      <div class="interior-header">
          <div class="interior-header__top">
              <h1 class="page-title interior-header__title">
                  Blog
                  <?php
                  if( is_paged() ){
                      $currentPageNum = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                      echo '- Page ' . $currentPageNum;
                  }
                  ?>
              </h1>            
          </div>
          <div class="interior-header__breadcrumbs">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                ?>
           </div>
      </div>
  
    <?php 
  }