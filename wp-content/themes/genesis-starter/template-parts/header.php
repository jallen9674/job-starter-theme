<?php 

/*----------------------------------
Global Header Content
-----------------------------------*/

function hennessey_global_header() {

    ?>

    <div class="site-header__notification-bar notification-bar">
        <div class="notification-bar__inner">
            <span class="notification-bar__left">
                Lorem Ipsum 
            </span>
            <span class="notification-bar__right">
                Dolor Sit 24/7
            </span>
        </div>
    </div>

   <div class="site-header__inner desktop-header">

        <div class="desktop-header__logo">

            <a href="<?php echo site_url(); ?>">
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-full.svg" alt="Logo" class="desktop-header__logo--default"/>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-only.svg" alt="Logo" class="desktop-header__logo--scrolled"/>
            </a>

        </div>

        <div class="desktop-header__nav">
            
            <?php //rs_custom_nav_menu(); ?>
            <nav role="navigation">
                <?php 
                    wp_nav_menu(
                        array(
                            'container' => false,                           
                            'container_class' => 'menu cf',                 
                            'menu' => 'All Pages',  
                            'menu_class' => 'desktop-nav',               
                            'theme_location' => 'Header Menu',                 
                            'before' => '',                                 
                            'after' => '',                                  
                            'link_before' => '',                            
                            'link_after' => '',                             
                            'depth' => 0,                                   
                            'fallback_cb' => ''                             
                        )
                    ); 
                ?>               
            </nav>

        </div>

        <div class="desktop-header__cta header-cta">
            <span class="header-cta__heading">
                Free Consultation
            </span>
            <span class="header-cta__subheading">
                    Call Us Anytime
            </span>
            <a class="header-cta__phone" href="tel:<?php echo hennessey_phone_display(); ?>">
                <?php echo hennessey_phone_display(); ?>
            </a>
        </div>

        <?php // Tablet Navigation TODO -- Better Implementation if Time?>
        
        <div class="tablet-navigation-wrapper">

            <nav role="navigation">
                <?php 
                    wp_nav_menu(
                        array(
                            'container' => false,                           
                            'container_class' => 'menu cf',                 
                            'menu' => 'All Pages',  
                            'menu_class' => 'desktop-nav',               
                            'theme_location' => 'main-nav',                 
                            'before' => '',                                 
                            'after' => '',                                  
                            'link_before' => '',                            
                            'link_after' => '',                             
                            'depth' => 0,                                   
                            'fallback_cb' => ''                             
                        )
                    ); 
                ?>               
            </nav>

        </div>

   </div>



    <?php 
}