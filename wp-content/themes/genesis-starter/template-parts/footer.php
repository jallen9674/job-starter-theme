<?php 
/*----------------------------------
Global Footer Content
-----------------------------------*/

function hennessey_global_footer() {
    ?>

    <footer class="site-footer">
      
        <div class="footer-contact-form">
            <div class="footer-contact-form__inner wrapper">

                <span class="footer-contact-form__title">
                    Contact Us Now
                </span>
                
                <span class="footer-contact-form__subtitle">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, accusamus!
                </span>

                <div class="footer-contact-form__form standard-form">
                    <?php echo do_shortcode('[contact-form-7 id="1709" title="Standard Contact Form"]'); ?>
                </div>

            </div>
        </div> <?php //End .footer-contact-form ?>

        
        <div class="footer-bottom">
            <div class="footer-bottom__inner wrapper">
            
                <div class="footer-bottom__navigation">
                    <ul>
                        <li><a href="<?php echo site_url(); ?>/sitemap/">Sitemap</a></li>&nbsp;&nbsp;|&nbsp;&nbsp;<li><a href="<?php echo site_url(); ?>/disclaimer/">Disclaimer</a></li>&nbsp;&nbsp;|&nbsp;&nbsp;<li><a href="<?php echo site_url(); ?>/privacy-policy/">Privacy Policy</a></li>
                    </ul>
                </div>

                <div class="footer-bottom__copyright">
                    &copy <?php echo date('Y'); ?> Site Name. All Rights Reserved.
                </div>

            </div>
        </div> <?php //End .footer-bottom ?>

    </footer>


    <?php 
}

