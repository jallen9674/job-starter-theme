<?php 
/*-------------------------------------
Hennessey Consulting Specific Functions
--------------------------------------*/

/*---------------------------------
Cleaning Up WP Head
----------------------------------*/
function removeHeadLinks() {
    remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
    remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
    remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
    remove_action( 'wp_head', 'index_rel_link' ); // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
    remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
  }
  add_action('init', 'removeHeadLinks');
  remove_action('wp_head', 'wp_generator');
  
if (  !is_user_logged_in() ){
  wp_deregister_style( 'dashicons' ); 
}

/*---------------------------------
Enqueue of Optimized Styles
----------------------------------*/
function hennessey_load_styles(){
    wp_enqueue_style( 'hennessey-css', get_stylesheet_directory_uri() .'/css/scss-styles.css' );
    wp_enqueue_style( 'hennessey-standalone-css', get_stylesheet_directory_uri() .'/css/standalone-styles.css' );
}
add_action( 'wp_enqueue_scripts', 'hennessey_load_styles', 10000);


/*---------------------------------
Enqueue of Optimized Scripts
----------------------------------*/

function hennessey_load_scripts(){
    wp_enqueue_script('hennessey-vendors-js', get_stylesheet_directory_uri() . '/js/vendors.js', array('jquery'), '1.0', true);  
    wp_enqueue_script('hennessey-custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), '1.0', true);
}
add_action( 'wp_enqueue_scripts', 'hennessey_load_scripts', 40);




/*--------------------------------------------
Favicons
---------------------------------------------*/

//Remove Genesis Favicon
remove_action('wp_head', 'genesis_load_favicon');

//Add Hennessey Favicon
function hennessey_add_favicon() {
    ?>
      <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/favicon-16x16.png">
      <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/favicons/site.webmanifest">
      <meta name="msapplication-TileColor" content="#2b5797">
      <meta name="theme-color" content="#ffffff">
    <?php
  }
  
  add_action('wp_head', 'hennessey_add_favicon');
  


/*---------------------------------
Bringing in Template Parts
----------------------------------*/

require_once('template-parts/footer.php');
require_once('template-parts/header.php');
require_once('template-parts/header-title-section.php');
require_once('template-parts/mobile-navigation-pane.php');


/*---------------------------------
Using Template Parts the Genesis Way
----------------------------------*/

//Adding Mobile Navigation Pane
add_action( 'genesis_after', 'hennessey_mobile_nav_pane' );
add_action( 'genesis_before', 'hennessey_mobile_nav_header' );

//Remove Genesis Header Content (Logo and Navigation)
remove_action( 'genesis_header', 'genesis_do_header' );
remove_action( 'genesis_header', 'genesis_do_nav' );

//Add Hennessey Header
add_action( 'genesis_header', 'hennessey_global_header' );

//Remove Genesis Footer
remove_action( 'genesis_footer', 'genesis_footer_markup_open', 5 );
remove_action( 'genesis_footer', 'genesis_do_footer' );
remove_action( 'genesis_footer', 'genesis_footer_markup_close', 15 );

//Add Hennessey Footer
add_action( 'genesis_footer', 'hennessey_global_footer' );


//Removing Interior Page Heading
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

//Adding Hennessey H1 & Breadcrumb Section
add_action('genesis_before_content', 'hennessey_single_interior_header');

/*---------------------------------
Post Types and Taxonomies
----------------------------------*/

require_once('post-types/faqs.php');
require_once('post-types/glossary.php');


/*---------------------------------
Adding Widgets
----------------------------------*/

require_once('widgets/related-pages/related-module.php');


/*---------------------------------
Modules
----------------------------------*/

//JSON-LD Module
require_once('modules/jsonld/init.php');


/*---------------------------------
Adding Shortcodes
----------------------------------*/

require_once('shortcodes/sidebar-form.php');
require_once('shortcodes/random-faq-text.php');
require_once('shortcodes/random-faq.php');

//Allow Sidebar Shortcodes
add_filter( 'widget_text', 'do_shortcode' );


/*---------------------------------
Image Sizes
----------------------------------*/

//Image Sizes
add_image_size( 'faq-thumb', 300, 200, true );
add_image_size( 'faq-thumb-sidebar', 400, 250, true );


/*---------------------------------
Add Read More Link to Post Excerpts
----------------------------------*/

add_filter('excerpt_more', 'get_read_more_link');
add_filter( 'the_content_more_link', 'get_read_more_link' );
function get_read_more_link() {
   return '... <a href="' . get_permalink() . '">Read More</a>';
}


/*---------------------------------
Adding CMB2
----------------------------------*/

require_once dirname( __FILE__ ) . '/lib/cmb2/init.php';

function update_cmb_meta_box_url( $url ) {
    $url = '/wp-content/themes/genesis-starter/lib/cmb2/';
    return $url;
}
add_filter( 'cmb2_meta_box_url', 'update_cmb_meta_box_url' );



/*---------------------------------
Setting Template Phone Number
----------------------------------*/

define("PHONE_NUMBER", "555-555-5555");

function hennessey_phone_display(){
  return PHONE_NUMBER;
}


/*--------------------------------------------
Adding Userway
---------------------------------------------*/

//add_action('wp_footer', 'hennessey_add_userway');

function hennessey_add_userway(){
  ?>
  <script type="text/javascript">
  var _userway_config = {
   account: 'PgJBLRHmZy'
  };
  </script>
  <script type="text/javascript" src="https://cdn.userway.org/widget.js"></script>
  <?php 
}


/*--------------------------------------------
Adding Google Analytics
---------------------------------------------*/

//add_action('wp_head', 'hennessey_add_analytics');

function hennessey_add_analytics(){
  ?>
  
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-34305819-10"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-34305819-10', { 'optimize_id': 'GTM-MCHP7BG'});
  </script>
  
  <?php 
}


/*--------------------------------------------
Adding Facebook Pixel 
---------------------------------------------*/

//add_action('wp_head', 'hennessey_add_fb');

function hennessey_add_fb(){
  ?>
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '850926998401561');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" alt="fb track" style="display:none"
    src="https://www.facebook.com/tr?id=850926998401561&ev=PageView&noscript=1"
  /></noscript>

  <?php
}



/*--------------------------------------------
Adding Call Tracking Metrics Snippet
---------------------------------------------*/

//add_action('wp_footer', 'hennessey_add_ctm');

function hennessey_add_ctm() {
  ?>
  <script async src="//165344.tctm.co/t.js"></script>
  <?php
}


/*--------------------------------------------
Adding Apex Chat
---------------------------------------------*/

//add_action('wp_footer', 'hennessey_add_apex');

function hennessey_add_apex() {  
  //Do Not Show During Local Development
  if ( !($_SERVER['SERVER_NAME']  == 'genesisstarter.local') ) {
  ?>
    <script src="//www.apex.live/scripts/invitation.ashx?company=Hennesseylaw" async></script>
  <?php 
  }
}


/*---------------------------------
Add Tag to Meta Description & Category
----------------------------------*/

add_filter( 'wpseo_metadesc', 'hennessey_description_tag', 100, 1 );

function hennessey_description_tag($s){
  if( is_tag() ){
     $s .= single_tag_title(" Posts Tagged as: ", false);
  }
  if( is_category() ){
     $s .= single_cat_title(" Posts Categorized as: ", false);
  }
  return $s;
 }
 
 
 /*------------------------------------------
 Add page number to meta description
 -------------------------------------------*/

 function hc_add_page_number( $s ) {
    global $page;
    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    ! empty ( $page ) && 1 < $page && $paged = $page;

    $paged > 1 && $s .= ' | ' . sprintf( __( 'Page: %s' ), $paged );

    return $s;
}

add_filter( 'wpseo_title', 'hc_add_page_number', 100, 1 );
add_filter( 'wpseo_metadesc', 'hc_add_page_number', 100, 1 );