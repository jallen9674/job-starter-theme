<?php
/**
 * Template Name: Homepage Template
 */


// Removes Skip Links.
remove_action( 'genesis_before_header', 'genesis_skip_links', 5 );

//Full Width Layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Remove Default the_content()
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Adding Homepage Layout
add_action( 'genesis_before_content',  'hennessey_homepage_layout');

//Adjusting Body Class
add_filter( 'body_class', 'hennessey_body_class' );
function hennessey_body_class( $classes ) {
	$classes[] = 'homepage-template';
	return $classes;
}

// Runs the Genesis loop.
genesis();


/*------------------------------
Homepage Layout
--------------------------------*/

function hennessey_homepage_layout(){
	?>
		
    <div class="homepage-section">
      <div class="homepage-section__inner content">
      
        <h2>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti tempora suscipit corporis nulla. Corporis blanditiis earum voluptates eum, maxime iure, quaerat quod atque qui nisi ullam sequi! Tempore repudiandae harum provident? Nam, quod sunt error maxime nobis libero a veritatis aut numquam soluta consectetur repellendus explicabo obcaecati suscipit, quis harum accusantium perspiciatis atque inventore temporibus unde deleniti. Obcaecati molestiae aspernatur odio porro nobis est at deleniti error assumenda mollitia inventore ea eius sit perferendis ratione repudiandae sint odit minus commodi, ad nam provident quis sapiente quidem! Alias reiciendis est, corporis nemo nisi labore facilis laudantium ex nam tempore. Deserunt expedita consectetur animi odio, iusto fugiat illum odit quis? Quia ipsa sint accusantium est beatae pariatur exercitationem sapiente sed iste quaerat.</p>

        <h2>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti tempora suscipit corporis nulla. Corporis blanditiis earum voluptates eum, maxime iure, quaerat quod atque qui nisi ullam sequi! Tempore repudiandae harum provident? Nam, quod sunt error maxime nobis libero a veritatis aut numquam soluta consectetur repellendus explicabo obcaecati suscipit, quis harum accusantium perspiciatis atque inventore temporibus unde deleniti. Obcaecati molestiae aspernatur odio porro nobis est at deleniti error assumenda mollitia inventore ea eius sit perferendis ratione repudiandae sint odit minus commodi, ad nam provident quis sapiente quidem! Alias reiciendis est, corporis nemo nisi labore facilis laudantium ex nam tempore. Deserunt expedita consectetur animi odio, iusto fugiat illum odit quis? Quia ipsa sint accusantium est beatae pariatur exercitationem sapiente sed iste quaerat.</p>

        <h2>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti tempora suscipit corporis nulla. Corporis blanditiis earum voluptates eum, maxime iure, quaerat quod atque qui nisi ullam sequi! Tempore repudiandae harum provident? Nam, quod sunt error maxime nobis libero a veritatis aut numquam soluta consectetur repellendus explicabo obcaecati suscipit, quis harum accusantium perspiciatis atque inventore temporibus unde deleniti. Obcaecati molestiae aspernatur odio porro nobis est at deleniti error assumenda mollitia inventore ea eius sit perferendis ratione repudiandae sint odit minus commodi, ad nam provident quis sapiente quidem! Alias reiciendis est, corporis nemo nisi labore facilis laudantium ex nam tempore. Deserunt expedita consectetur animi odio, iusto fugiat illum odit quis? Quia ipsa sint accusantium est beatae pariatur exercitationem sapiente sed iste quaerat.</p>


      </div>
    </div>
   
	<?php 
}
