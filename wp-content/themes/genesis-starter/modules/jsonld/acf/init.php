<?php
$acf_exists = class_exists('acf');

// Check if ACF is used with another plugin, if not already called, use this one
if( !$acf_exists ) {
  // Load the ACF Core
  require_once('advanced-custom-fields/acf.php');
}

// Repeater Add-On
if( ! class_exists('acf_repeater_plugin') ) {
  require_once('addons/acf-repeater/acf-repeater.php');
}

// Options Add-on
if( ! class_exists('acf_options_page_plugin') ) {
  require_once('addons/acf-options-page/acf-options-page.php');

  // Remove the defualt "Options" Page from ACF as it would be included within the plugin menu or elsewhere.
  /*if( ! function_exists("remove_acf_options_page")) {
  function remove_acf_options_page() {
  remove_menu_page('acf-options');
}
add_action('admin_menu', 'remove_acf_options_page', 99);
}*/
}

if( !$acf_exists ) {

  add_filter('acf/settings/path', 'my_acf_settings_path');

  function my_acf_settings_path( $path ) {

      // update path
      $path = get_stylesheet_directory() . '/hennessey/modules/jsonld/acf/advanced-custom-fields/';

      // return
      return $path;

  }


  // 2. customize ACF dir
  add_filter('acf/settings/dir', 'my_acf_settings_dir');

  function my_acf_settings_dir( $dir ) {

      // update path
      $dir = get_stylesheet_directory_uri() . '/hennessey/modules/jsonld/acf/advanced-custom-fields/';

      // return
      return $dir;

  }
}

// Menu Select Add-on
if( ! class_exists( 'ACF_Nav_Menu_Field_Plugin' ) ) {
  require_once('addons/advanced-custom-fields-nav-menu-field/fz-acf-nav-menu.php');
}

// Menu Select Add-on
if( ! class_exists( 'acf_field_taxonomy_chooser' ) ) {
  require_once('addons/acf-term-and-taxonomy-chooser/acf-taxonomy-chooser.php');
}

// Menu Select Add-on
if( ! class_exists( 'acf_field_posttype_select' ) ) {
  require_once('addons/post-type-select-for-advanced-custom-fields/acf-posttype-select.php');
}

//that should be variable
$glossary_post_type = 'hc_gallery';

// Add Options for Glossary Archive
if( function_exists('acf_add_options_page') )
{
    acf_add_options_page(array(
        'page_title'    => 'Glossary JSON-LD',
        'menu_title'    => 'Glossary JSON-LD',
        'menu_slug'     => 'options_glossary_json_ld',
        'capability'    => 'edit_posts',
        'parent_slug'   => 'edit.php?post_type=' . $glossary_post_type,
        'position'      => false,
        'icon_url'      => 'dashicons-images-alt2',
        'redirect'      => false,
    ));
}

// Adding the ACF fields
require_once('fields/options.php');
require_once('fields/single.php');
