<?php

function hc_video( $atts ) {

	$atts = shortcode_atts(
		array(
			'url' => '',
			'name' => '',
			'duration' => '',
			'thumbnail' => '',
			'upload_date' => '',
			'description' => '',
			'width' => 'full'
		),
		$atts
	);

	//26-12-2018
	//PT02H22M11S
	//width: full and half

	$json = '<script type="application/ld+json">
				{
				  "@context": "http://schema.org/",
				  "@type": "VideoObject",
				  "name": '. json_encode($atts['name']) .',
				  "contentUrl": "'. $atts['url'] .'",
				  "duration": "PT'. $atts['duration'] .'",
				  "thumbnailUrl": "'. $atts['thumbnail'] .'",
				  "uploadDate": "'. $atts['upload_date'] .'",
				  "description": '. json_encode($atts['description']) .'
				}
				</script>';

	$html = '<div class="'. $atts['width'] .'-width-hcvideo">'. wp_oembed_get($atts['url']) . $json .'</div>';

	echo $html;
}

add_shortcode( 'hcvideo', 'hc_video' );

function hc_video_schema_only( $atts ) {

	$atts = shortcode_atts(
		array(
			'url' => '',
			'name' => '',
			'duration' => '',
			'thumbnail' => '',
			'upload_date' => '',
			'description' => ''
		),
		$atts
	);

	$json = '<script type="application/ld+json">
				{
				  "@context": "http://schema.org/",
				  "@type": "VideoObject",
				  "name": '. json_encode($atts['name']) .',
				  "contentUrl": "'. $atts['url'] .'",
				  "duration": "PT'. $atts['duration'] .'",
				  "thumbnailUrl": "'. $atts['thumbnail'] .'",
				  "uploadDate": "'. $atts['upload_date'] .'",
				  "description": '. json_encode($atts['description']) .'
				}
				</script>';

	echo $json;
}

add_shortcode( 'hcvideoschema', 'hc_video_schema_only' );

add_action( 'init', function () {
    wp_oembed_add_provider( '/https?:\/\/(.+)?(wistia\.com|wi\.st)\/(medias|embed)\/.*/', 'http://fast.wistia.net/oembed', true );
});

function add_stylesheet_to_head() {
    echo "<style type='text/css'>
			.full-width-hcvideo {
			    position: relative;
				padding-bottom: 56.25%;
				padding-top: 30px;
				height: 0;
				overflow: hidden;
				max-width: 100%;
				height: auto;
			}

			.half-width-hcvideo {
			    position: relative;
				padding-bottom: 25.25%;
				padding-top: 30px;
				height: 0;
				overflow: hidden;
				max-width: 50%;
				width: 50%;
				height: auto;
				float: left;
			}

			.full-width-hcvideo iframe,
			.full-width-hcvideo object,
			.full-width-hcvideo embed,
			.half-width-hcvideo iframe,
			.half-width-hcvideo object,
			.half-width-hcvideo embed{
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
			}
		  </style>";
}

add_action( 'wp_head', 'add_stylesheet_to_head' );

function addVideoSchema( $url, $name, $duration, $thumbnail, $upload_date, $description, $width = 'full') {
	$json = '<script type="application/ld+json">
				{
				  "@context": "http://schema.org/",
				  "@type": "VideoObject",
				  "name": '. json_encode($name) .',
				  "contentUrl": "'. $url .'",
				  "duration": "PT'. $duration .'",
				  "thumbnailUrl": "'. $thumbnail .'",
				  "uploadDate": "'. $upload_date .'",
				  "description": '. json_encode($description) .'
				}
			</script>';

	echo $json;
}
