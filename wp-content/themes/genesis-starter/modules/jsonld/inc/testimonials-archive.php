<?php
function jsonldTestimonialsArchive()
{
  	global $post;
	$thePostID = $post->ID;

	if (get_field('schema_type_json', $thePostID)) {
		$schema_type = get_field('schema_type_json', $thePostID);
	}
	// Display Schema Type only on specific themplate - JSON-LD-Post Group
	if ($schema_type == 'testimonialspage') {	
    $rev = get_field('archive_testimonials_json', $thePostID);
	$serviceType = $rev['testimonials_service_type_json'];
    $local_section = $rev['testimonial_group_json'];
	$ratingTMP = 0;
	$r = 1;
	foreach($local_section as $row) {	
	$author = $row['testimonial_author'];
	$datePublished = $row['testimonial_published_json'];
	$reviewBody = $row['testimonial_content_json'];
    $ratingValue = $row['rating'];
	$ratingTMP += $ratingValue;
	$rows .= '{
	"@type": "Review",
			"author": "'. $author .'",
			"datePublished": "'. $datePublished .'",
			"reviewBody": "'. $reviewBody .'",
		"reviewRating": {
            "@type": "Rating",
            "bestRating": "5",
            "ratingValue": "'. $ratingValue .'",
            "worstRating": "1"
		}
	}';	
			if ($r < count($local_section)) {
			  $rows .= ',';
			}
	$r++;
	}
	
	$ratingCount = count($local_section);
	$ratingValue = $ratingTMP/$ratingCount;
	$review_service_name_organization = get_field('name_json', 'options');
	
	$html = '<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Service",    
  "serviceType": "'. $serviceType .'",
      "provider": {
        "@type": "Organization",
        "name": '. json_encode($review_service_name_organization) .'
      },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "'. $ratingValue .'",
    "ratingCount": "'. $ratingCount .'"
      },
  "review":
    
	['. $rows .']
 
}
</script>
';
	
	echo $html;
}
}
