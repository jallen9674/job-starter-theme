<?php
function jsonldOrg()
{
	if (empty(get_field('name_json', 'options'))) {
		return;
	}

/* Founders */

$founders = '';
$founders_field = get_field('founders_json', 'options');
$founders_count = count($founders_field);
$i = 1;

foreach($founders_field as $founder) {

	$founders .= '{
		"@type": "Person",
		"name": '. json_encode($founder['founder_json']) .'
	  }';

	if ($i < $founders_count) {
	  $founders .= ',';
	}

	$i++;
}

/* Social Networks */

$sameAs = '';
$sameAs_field = get_field('sameas_json', 'options');
$sameAs_count = count($sameAs_field);
$s = 1;

foreach($sameAs_field as $sameass) {

	$sameAs .= ''. json_encode($sameass['sameass_json']) .'';

	if ($s < $sameAs_count) {
	  $sameAs .= ',';
	}

	$s++;
}



$contactPoint = '';
$contactPoint_field = get_field('contactpoint_json', 'options');
$contactPoint_count = count($contactPoint_field);
$d = 1;

foreach($contactPoint_field as $row) {

	$contactPoint .= '{
		"@type": "contactPoint",
		"contactType": "'.$row['choosecstype_json'].'",
		"telephone":  '. json_encode($row['contacttypetelephone_json']) .' ,
		"email":  '. json_encode($row['email_json']) .'
	  }';

	if ($d < $contactPoint_count) {
	  $contactPoint .= ',';
	}

	$d++;
}
// Fix a bug with diff chars like double quotes in Options Field
$jname = get_field('name_json', 'options');
$foundingDate = get_field('foundingdate_json', 'options');
$legalName = get_field('legalname_json', 'options');
$streetAddress = get_field('streetaddress_json', 'options');
$addressLocality = get_field('addresslocality_json', 'options');
$addressRegion = get_field('addressregion_json', 'options');
$postalCode = get_field('postalcode_json', 'options');
$addressCountry = get_field('addresscountry_json', 'options');
// End fix

$html = '<script type="application/ld+json">
{ "@context": "http://schema.org",
"@type": "Organization",
"name": '. json_encode($jname) .',
"legalName" : '. json_encode($legalName) .',
"url": "'. get_home_url() .'",
"logo": "'. get_field('logo_json', 'options') .'",
"foundingDate":  '. json_encode($foundingDate) .',
"founders": ['. $founders .'],
  "address": {
    "@type": "PostalAddress",
    "streetAddress":   '. json_encode($streetAddress) .',
    "addressLocality": '. json_encode($addressLocality) .',
    "addressRegion": '. json_encode($addressRegion) .',
    "postalCode": '. json_encode($postalCode) .',
    "addressCountry": '. json_encode($addressCountry) .'
  },
  "contactPoint": ['. $contactPoint .'],
    "sameAs" : ['. $sameAs .']
  }
  </script>';

	echo $html;
}
