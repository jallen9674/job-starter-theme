<?php
function jsonldLocalBusiness()
{
  global $post;
  $thePostID = $post->ID;

  if (get_field('schema_type_json', $thePostID)) {
    $schema_type = get_field('schema_type_json', $thePostID);
  }

  if ($schema_type == 'localbusiness') {
    $description = hc_strip_shortcodes(wpautop( get_the_content( $thePostID )));
    $short_description = substr( $description, 0, strpos( $description, '</p>' ) + 4 );
    $short_description = wp_strip_all_tags($short_description);
    $short_description = json_encode($short_description);
    $description = wp_strip_all_tags($description);
    $description = json_encode($description);
    $image = get_the_post_thumbnail_url($thePostID, 'large');
    $the_title = json_encode(get_the_title($thePostID));
    $local_bussines = get_field('local_bussines', $thePostID);

    // Get Opening Hours from Options and Local Pages

    $openingHoursArrayOptions = get_field('openingHours_json', 'options');
    $openingHoursArrayLocal = $local_bussines['openinghourslb_json'];


    $priceRange = $local_bussines['pricerangelb_json'];


    // Image

    if(empty($image)) {
      ob_start();
      ob_end_clean();
      $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
      $image = $matches[1][0];
    }

    if(empty($image)) {
      $image = get_field('default_post_thumbnail', 'options');
    }

    $i = 1;

    // Address Locality Denton for example

    if( empty($local_bussines['addressLocalitylb_json'])) {
      $addresslocality = get_field('addressLocality_json', 'options');
    } else {
      $addresslocality = $local_bussines['addressLocalitylb_json'];
    }

    // Address Region DL for Example

    if( empty($local_bussines['addressregionlb_json'])) {
      $addressregion = get_field('addressregion_json', 'options');
    } else {
      $addressregion = $local_bussines['addressregionlb_json'];
    }

    // Local Postal Code Example

    if( empty($local_bussines['postalcodelb_json'])) {
      $postalcode = get_field('postalcode_json', 'options');
    } else {
      $postalcode = $local_bussines['postalcodelb_json'];
    }

    // Local Street Address
    // If Local Field is Empty Display Default Local Address else Display The Local Field
    if( empty($local_bussines['streetaddresslb_json']) ) {
      $streetAddress = get_field('streetaddress_json', 'options');
    } else {
      $streetAddress = $local_bussines['streetaddresslb_json'];
    }

    // Local Telephone
    //var_dump($local_bussines['telephonelb_json']);
    if( empty($local_bussines['telephonelb_json'])) {
      $phone = get_field('mainphone_json', 'options');
    } else {
      $phone = $local_bussines['telephonelb_json'];
    }

    // Price Range
    // Disable Multiple Options select in ACF Plugin
    if( $priceRange ){
      foreach($priceRange as $priceRange) {
        ''.$priceRange['pricerangelb_json'].'';
      }
    }


    // always good to see exactly what you are working with

    // If Local Opening Hours are empty display default Opening Hours else display Local Opening Hours
    // Local Opening Hours
    if( empty($openingHoursArrayLocal)) {
      $openingHoursTmp = $openingHoursArrayOptions;
      $openingHours = '';
      $i = 1;
      foreach($openingHoursTmp as $hours) {
        $openingHours .= $hours['label'];

        if ($i < count($openingHoursTmp)) {
          $openingHours .= ',';
        }

        $i++;
      }
      $openingHours .= ' '. get_field('workingtimesstart_json', 'options') .'-'. get_field('working_time_end_json', 'options') .'';
    } else {
      $openingHours = $local_bussines['openingHourslb_json'];
      $openingHours .= ' '. $local_bussines['workingtimesstartlb_json'] .'-'. $local_bussines['working_time_end_lb_json'] .' ';

      $openingHoursTmp = $openingHoursArrayLocal;
      $openingHours = '';
      $i = 1;
      foreach($openingHoursTmp as $hours) {
        $openingHours .= $hours;

        if ($i < count($openingHoursTmp)) {
          $openingHours .= ',';
        }

        $i++;
      }
      $openingHours .= ' '. $local_bussines['workingtimesstartlb_json'] .'-'. $local_bussines['working_time_end_lb_json'] .' ';
    }


    // GeoCoordinates
    if( empty(get_field('latitudelb_json')) ) {
      $latitude = $local_bussines['latitudelb_json'];
    } else {
      $latitude = get_field('latitudelb_json');
    }

    if( empty(get_field('longitudelb_json')) ) {
      $longitude = $local_bussines['longitudelb_json'];
    } else {
      $longitude = get_field('longitudelb_json');
    }



    $sameAs = '';
    $sameAs_field = get_field('sameas_json', 'options');
    $sameAs_count = count($sameAs_field);
    $s = 1;

    foreach($sameAs_field as $sameass) {

      $sameAs .= ''. json_encode($sameass['sameass_json']) .'';

      if ($s < $sameAs_count) {
        $sameAs .= ',';
      }

      $s++;
    }

    $use_fixed = $local_bussines['use_fixed_location_or_radius'];

    if ($use_fixed) {
      $loc_html = '"geo": {
        "@type": "GeoCoordinates",
        "latitude": '. json_encode($latitude) .',
        "longitude": '. json_encode($longitude) .'
      },';
    } else {
      $loc_html = '"location": {
        "@type": "Place",
        "geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": '. json_encode($local_bussines['radius_lat']) .',
            "longitude": '. json_encode($local_bussines['radius_center_longtitude']) .'
          },
          "geoRadius": '. json_encode($local_bussines['radius_in_miles']) .'
        }
      },';
    }
    $html = '<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "LocalBusiness",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": '. json_encode($addresslocality) .',
        "addressRegion": '. json_encode($addressregion) .',
        "postalCode": '. json_encode($postalcode) .',
        "streetAddress": '. json_encode($streetAddress) .'
      },
      "description": '. $short_description .',
      "image": "'. $image .'",
      "name": '. $the_title .',
      "telephone": '. json_encode($phone) .',
      "openingHours": "'. $openingHours .'",
      "priceRange": "'. $priceRange .'",
      '. $loc_html .'
      "sameAs" : ['. $sameAs .']
    }
    </script>';

    echo $html;
  }
}
