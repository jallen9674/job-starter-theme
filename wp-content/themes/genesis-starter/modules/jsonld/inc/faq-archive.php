<?php
function jsonldFAQarchive()
{
  global $post;
  $thePostID = $post->ID;

  if (get_field('schema_type_json', $thePostID)) {
    $schema_type = get_field('schema_type_json', $thePostID);
  }

  if ($schema_type == 'faqarchive') {

    $faqarchive_data = get_field('faq_archive_json', $thePostID);
    $post_type = $faqarchive_data['post_type_faq_json'] ?: get_field('global_faq_post_type_json', 'option');
    $taxonomy = $faqarchive_data['taxonomy_faq_json'] ?: get_field('global_faq_taxonomy_json', 'option');

    if (empty($post_type) || empty($taxonomy)) {
      return;
    }

    $faqTerms = get_terms( $taxonomy );
    // convert array of term objects to array of term IDs
    $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

    $args = array(
      'posts_per_page' => -1,
      'post_type' => $post_type,
      'tax_query' => array(
        array(
          'taxonomy' => $taxonomy,
          'field' => 'term_id',
          'terms' => $faqTermIDs
        ),
      ),
      'order' => 'DSC',
    );

    $a_query = new WP_Query( $args );
    if (!$a_query->have_posts()) {
      return;
    }
    $qa_json_partition = '';
    $i = 1;
    $posts_count = $a_query->post_count;

    while ( $a_query->have_posts() ) : $a_query->the_post();
    $qPostID = $post->ID;
    $description = hc_strip_shortcodes(wpautop( get_the_content( $qPostID ) ));
    $description = wp_strip_all_tags($description);
    $description = json_encode($description);
    $page_url = get_the_permalink($qPostID);
    $the_title = json_encode(get_the_title($qPostID));
    $date_published = get_the_date('d-m-Y',$qPostID);

    $qa_json_partition .= '
    {
      "@type": "Question",
      "name": '.$the_title.',
      "acceptedAnswer": {
        "@type": "Answer",
        "text": '.$description.'
      }
    }';

    if ($i < $posts_count) {
      $qa_json_partition .= ',';
    }

    $i++;

  endwhile;

  wp_reset_query();
  $html = '<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "FAQPage",
    "mainEntity": ['. $qa_json_partition .']
  }
  </script>';

  echo $html;

}
}
