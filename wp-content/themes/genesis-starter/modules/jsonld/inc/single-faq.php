<?php
function jsonldSingleFAQ()
{
  global $post;
  $thePostID = $post->ID;
  $is_matching = false;

  if (get_field('schema_type_json', $thePostID)) {
    $schema_type = get_field('schema_type_json', $thePostID);
  }

  if (get_field('global_faq_post_type_json', 'options')) {
    $faq_post_type = get_field('global_faq_post_type_json', 'options');
    $post_type = get_post_type($thePostID);
    if ($faq_post_type == $post_type) {
      $is_matching = true;
    }
  }
  
  if ($faq_post_type && get_field('target_a_specific_taxonomy', 'options') == true) {
    $faq_post_tax_options = get_field('global_faq_taxonomy_json', 'options');
    $faq_post_tax = get_post_taxonomies($thePostID);
    $get_terms = wp_get_post_terms($thePostID, $faq_post_tax);
    $is_matching_test = array();

    foreach ($get_terms as $term) {
      $current_taxonomy = $term->taxonomy;
      //var_dump($current_taxonomy);
      //var_dump($faq_post_tax_options);
      if ($faq_post_tax_options == $current_taxonomy) {
        array_push($is_matching_test, 'true');
      } else {
        array_push($is_matching_test, 'false');
      }
    }
      if (in_array('true', $is_matching_test)) {
        $is_matching = true;
      } else {
        $is_matching = false;
      }
  }

  if ( $schema_type == 'singlefaq' || $is_matching ) {
    $description = hc_strip_shortcodes(wpautop( get_the_content( $thePostID )));
    $description = wp_strip_all_tags($description);
    $description = json_encode($description);
    $page_url = get_the_permalink($thePostID);
    $the_title = json_encode(get_the_title($thePostID));
    $date_published = get_the_date('d-m-Y',$thePostID);
    $image = get_the_post_thumbnail_url($thePostID, 'large');

    if(empty($image)) {
      ob_start();
      ob_end_clean();
      $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
      $image = $matches[1][0];
    }
    if(empty($image)) {
      $image = get_field('default_post_thumbnail', 'options');
    }

    if(!empty($image)) {
      $image_size = getimagesize($image);

      $image_html = '"primaryImageOfPage": {
        "@type": "ImageObject",
        "contentUrl": "'. $image .'",
        "width": "'. $image_size[0] .'",
        "height": "'. $image_size[1] .'"
      }';
    }

    $html = '<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@graph": [
        {
          "@type": "Question",
          "text": '. $the_title .',
          "dateCreated": "'. $date_published .'",
          "acceptedAnswer": {
            "@type": "Answer",
            "text": '. $description .',
            "dateCreated": "'. $date_published .'"
          }
        },
        {
          "@type": "FAQPage",
          "name": '. $the_title .',
          "text": '. $description .',
          '. $image_html .'
        }
      ]
    }
    </script>';

    echo $html;
  }
}
