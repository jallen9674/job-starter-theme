<?php
function jsonldSingleTestimonial()
{

global $post;
	$thePostID = $post->ID;

	if (get_field('schema_type_json', $thePostID)) {
		$schema_type = get_field('schema_type_json', $thePostID);
	}

	if ($schema_type == 'singletestimonial') {
        $description = hc_strip_shortcodes(wpautop( get_the_content( $thePostID )));
		$short_description = substr( $description, 0, strpos( $description, '</p>' ) + 4 );
		$short_description = wp_strip_all_tags($short_description);
		$short_description = json_encode($short_description);
		$description = wp_strip_all_tags($description);
		$description = json_encode($description);
		$testimonialGroup = get_field('single_testimonial_json', $thePostID);
		$ratingValue = $testimonialGroup['ratingvalue_json'];
        $reviewGroup = get_field('single_testimonial_json', $thePostID);
		$reviewer = $reviewGroup['reviewername_json'];
		$stypegroup = get_field('single_testimonial_json', $thePostID);
		$serviceType = $stypegroup['servicetype_json'];
		
		
		// Choose Service Type
		if(!empty($stypegroup['servicetype_json'])) {
			$stype = $stypegroup['servicetype_json'];

		} else {
			$stype = $stypegroup['servicetype_json'];

		}
		
	    // Name of the Person who lefy review
		if(!empty($reviewGroup['reviewername_json'])) {
			$rname = $reviewGroup['reviewername_json'];
		} else {
			$rname = $reviewGroup['reviewername_json'];
		}

	// Add rating score to Specific Service Type
		if(!empty($ratingValue['value'])) {
			$ratingValueStars = $ratingValue['value'];
		} else {
			$ratingValueStars = '5';
		}
	// Autor of the post review
	
	if( empty(get_field('author_json', 'options')) ) {
		$author = json_encode(get_the_author_meta('display_name'));
	} else {
		$author = json_encode(get_field('author_json', 'options'));
	}
	
$review_service_name_organization = get_field('name_json', 'options');	
	
$html = '<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Review",
  "itemReviewed": {
    "@type": "Service",
    "serviceType": "'. $stype .'" ,
      "provider": {
        "@type": "Organization",
        "name": '. json_encode($review_service_name_organization) .'
      }
  },
  "reviewRating": {
    "@type": "Rating",
    "ratingValue": '. $ratingValueStars .'
  },
  "name": "'. $rname .'",
  "author": {
    "@type": "Person",
    "name":  '. $author .'
  },
  "reviewBody": '. $short_description .' ,
  "publisher": {
    "@type": "Organization",
    "name": '. json_encode($review_service_name_organization) .'
  }
}
</script>';
	
	echo $html;
}
}
