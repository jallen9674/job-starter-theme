<?php
function jsonldService()
{
  	global $post;
	$thePostID = $post->ID;

	if (get_field('schema_type_json', $thePostID)) {
		$schema_type = get_field('schema_type_json', $thePostID);
	}

	//if ($schema_type == 'blog' || get_field('schema_type', 'options')) TODO: make this functionality working with overwrite if cpt is selected
	if ($schema_type == 'service') {
		$description = hc_strip_shortcodes(wpautop( get_the_content( $thePostID )));
		$short_description = substr( $description, 0, strpos( $description, '</p>' ) + 4 );
		$short_description = wp_strip_all_tags($short_description);
		$short_description = json_encode($short_description);
		$description = wp_strip_all_tags($description);
		$description = json_encode($description);
		$page_url = get_the_permalink($thePostID);
		$the_title = json_encode(get_the_title($thePostID));
		$date_published = get_the_date('d-m-Y',$thePostID);
		$image = get_the_post_thumbnail_url($thePostID, 'large');
		$services_data = get_field('services_data_json', $thePostID);
		$local_sections = $services_data['sections_json'];
		if(empty($image)) {
			ob_start();
			ob_end_clean();
			$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
			$image = $matches[1][0];
		}
		if(empty($image)) {
			$image = get_field('logo_json', 'options');
		}

		$i = 1;
		foreach($local_sections as $local_section) {

			$local_title = json_encode($local_section['offer_name_json']);
			$local_short_desc = json_encode($local_section['offer_short_desc_json']);
			$local_desc = json_encode(wp_strip_all_tags($local_section['offer_desc_json']));

			$local_page_data .= '{
					"@type": "Offer",
					"itemOffered": {
					  "@type": "Service",
					  "name": '. $local_title .',
					  "disambiguatingDescription": '. $local_short_desc .',
					  "description": '. $local_desc .'
					}
				  }';

			if ($i < count($local_sections)) {
			  $local_page_data .= ',';
			}

			$i++;
		}

		$children_pages_data = $services_data['children_services'];
		foreach ($children_pages_data as $page_data) {
			$childShorTitle = json_encode($page_data['short_name_child_service']);
			$pageArr = $page_data['link_child_service'];
			$postID = $pageArr->ID;

			$description_p = wpautop( $pageArr->post_content );
			$short_description_p = substr( $description_p, 0, strpos( $description_p, '</p>' ) + 4 );
			$short_description_p = wp_strip_all_tags($short_description_p);
			$short_description_p = json_encode($short_description_p);
			$p_title = json_encode(get_the_title($postID));;
			$page_url_p = get_the_permalink($postID);
			$image_p = get_the_post_thumbnail_url($postID, 'large');

			if(empty($image)) {
				ob_start();
				ob_end_clean();
				$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $description_p, $matches);
				$image = $matches[1][0];
			}
			if(empty($image)) {
			$image = get_field('default_post_thumbnail', 'options');
		}


			$children_html .= ', {
					"@type": "Offer",
					"itemOffered": {
					  "@type": "Service",
					  "name": '. $childShorTitle .',
					  "alternateName": '. $p_title .',
					  "description": '. $short_description_p .',
					  "url": "'. $page_url_p .'",
					  "image": "'. $image_p .'"
					}
				  }';
		}
		$orgname = get_field('name_json', 'options');

		//$children_html ='';
		$html = '<script type="application/ld+json">
			{
			  "@context": "http://schema.org/",
			  "@type": "Service",
			  "serviceType": '. json_encode(get_field('servicetype_json', 'options')) .',
			  "provider": {
				"@type": "Organization",
				"name": '. json_encode($orgname) .'
			  },
			  "hasOfferCatalog": {
				"@type": "OfferCatalog",
				"name": '. json_encode($services_data['short_title_json']) .',
				"alternateName": '. $the_title .',
				"description": '. $short_description .',
				"url": "'. $page_url .'",
				"image": "'. $image .'",
				"itemListElement": [
				  '. $local_page_data .'
				  '. $children_html .'
				]
			  }
			}
			</script>
			';

		echo $html;
	}
}
