<?php
function jsonldDefinedTermSet()
{

  if (is_archive() || is_post_type_archive()) {
    $term = get_queried_object();
    $queried_object = get_queried_object();
    $taxonomy = $queried_object->taxonomy;
    $term_id = $queried_object->term_id;

    $acf_backend_type = $taxonomy . '_' . $term_id;
    $schema_type = get_field('schema_type_json', $acf_backend_type);

    if(!$schema_type) {
      $acf_backend_type = 'option';
      $schema_type = get_field('schema_type_json', $acf_backend_type);
    }

    if ($schema_type != 'definedtermset') {
      return;
    }

    $DefinedTermSetname = get_field('defined_term_set', $acf_backend_type);
    $DefinedTermSetname = $DefinedTermSetname['defined_term_set_name'];
    $the_title = json_encode($queried_object->name);
    $rowdef = get_field('defined_term_set', $acf_backend_type);
    $defsetTMP = $rowdef['select_post_to_display_defined_term_set_schema'];

    if( is_post_type_archive() ) {
      $page_url_set = get_post_type_archive_link( $queried_object->post_type );
    } elseif ( is_archive() ) {
      $page_url_set = get_term_link( $term_id, $taxonomy );
    }

    if(!$page_url_set) {
      $page_url_set = get_field('main_glossary_url', 'options');
    }

  } else {
    global $post;
    $thePostID = $post->ID;

    if (get_field('schema_type_json', $thePostID)) {
      $schema_type = get_field('schema_type_json', $thePostID);
    }
    if ($schema_type != 'definedtermset') {
      return;
    }

    $page_url_set = get_the_permalink($thePostID);
    $DefinedTermSetname = get_field('defined_term_set', $thePostID);
    $DefinedTermSetname = $DefinedTermSetname['defined_term_set_name'];
    $the_title = get_the_title($thePostID);
    $rowdef = get_field('defined_term_set', $thePostID);
    $defsetTMP = $rowdef['select_post_to_display_defined_term_set_schema'];
  }

  if ($schema_type == 'definedtermset') {

    $r = 1;
    foreach($defsetTMP as $rowdef) {
      $page_url_set_def = get_the_permalink($rowdef);
      $the_title_def = get_the_title($rowdef);

      $rowdefhtml .= '{
        "@type": "DefinedTerm",
        "@id": "'.$page_url_set_def .'",
        "name": "'. $the_title_def .'",
        "inDefinedTermSet":  "'. $page_url_set .'"
      }';
      if ($r < count($defsetTMP)) {
        $rowdefhtml .= ',';
      }
      $r++;
    }


    $html = '<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@graph": [

        {
          "@type": "DefinedTermSet",
          "@id": "'.$page_url_set .'",
          "name": '. json_encode($DefinedTermSetname) .'
        },
        '. $rowdefhtml .'
      ]
    }
    </script>';

    echo $html;
  }

}
