<?php
function jsonldLegalService()
{
	if (empty(get_field('name_json', 'options'))) {
		return;
	}
	
global $post;

		// Get Opening Hours from Options and Local Pages

		$openingHoursArrayOptions = get_field('openingHours_json', 'options');
		$openingHoursArrayLocal = $local_bussines['openinghourslb_json'];

// Price range
        $range = get_field('default_price_range_json', 'options');

        if( $range ){
        foreach($range as $range) {
        ''.$range['default_price_range_json'].'';
	    }
        }


// id URL on the top of JSON-ld scheme for LegalService



// Top Image for Legal Service
$image = get_the_post_thumbnail_url($thePostID, 'large');
		if(empty($image)) {
			ob_start();
			ob_end_clean();
			$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
			$image = $matches[1][0];
		}

		if(empty($image)) {
			$image = get_field('default_post_thumbnail', 'options');
		}

		$i = 1;
//Legal Service Description
$description = get_field('legal_service_description_json', 'options');

// Image
$jname = get_field('name_json', 'options');
$email = get_field('legal_email_address_json', 'options');

// Founders

$founders = '';
$founders_field = get_field('founders_json', 'options');
$founders_count = count($founders_field);
$f = 1;

foreach($founders_field as $row) {


		 $foundername = $row['legal_service_founder_name_json'];
         $naics = $row['naics_code_json'];
         $jobTitle = $row['legal_job_title_json'];
         $gender = $row['founders_gender_json'];
         $alumniof = $row['alumni_of_json'];
		 $founderurl = $row['founder_website_json'];

	     $founders .= '{
		    "url": '. json_encode($founderurl) .',
		    "name": '. json_encode($foundername) .',
			"jobTitle": '. json_encode($jobTitle) .',
			"naics": '. json_encode($naics) .',
			"gender": '. json_encode($gender) .',
		    "alumniOf": {
		        "@type": "Organization",
	            "name": '. json_encode($alumniof) .'
		                }
	  }';

	if ($f < $founders_count) {
	  $founders .= ',';
	}

	$f++;
}

$contactPoint = '';
$contactPoint_field = get_field('contactpoint_json', 'options');
$contactPoint_count = count($contactPoint_field);
$d = 1;

foreach($contactPoint_field as $row) {

	$contactPoint .= '{
		"@type": "contactPoint",
		"contactType": "'.$row['choosecstype_json'].'",
		"telephone":  '. json_encode($row['contacttypetelephone_json']) .' ,
		"email":  '. json_encode($row['email_json']) .'
	  }';

	if ($d < $contactPoint_count) {
	  $contactPoint .= ',';
	}

	$d++;
}


$sameAs = '';
$sameAs_field = get_field('sameas_json', 'options');
$sameAs_count = count($sameAs_field);
$s = 1;

foreach($sameAs_field as $sameass) {

	$sameAs .= ''. json_encode($sameass['sameass_json']) .'';

	if ($s < $sameAs_count) {
	  $sameAs .= ',';
	}

	$s++;
}

 // Local Opening Hours
		if( empty($openingHoursArrayLocal)) {
			$openingHoursTmp = $openingHoursArrayOptions;
			$openingHours = '';
			$i = 1;
			foreach($openingHoursTmp as $hours) {
				$openingHours .= $hours['label'];

				if ($i < count($openingHoursTmp)) {
					$openingHours .= ',';
				}

				$i++;
			}
			$openingHours .= ' '. get_field('workingtimesstart_json', 'options') .'-'. get_field('working_time_end_json', 'options') .'';
		} else {
			$openingHours = $local_bussines['openingHourslb_json'];
			$openingHours .= ' '. $local_bussines['workingtimesstartlb_json'] .'-'. $local_bussines['working_time_end_lb_json'] .' ';

			$openingHoursTmp = $openingHoursArrayLocal;
			$openingHours = '';
			$i = 1;
			foreach($openingHoursTmp as $hours) {
				$openingHours .= $hours;

				if ($i < count($openingHoursTmp)) {
					$openingHours .= ',';
				}

				$i++;
			}
			$openingHours .= ' '. $local_bussines['workingtimesstartlb_json'] .'-'. $local_bussines['working_time_end_lb_json'] .' ';
		}
// Fix a bug with diff chars like double quotes in Options Field
$streetAddress = get_field('streetaddress_json', 'options');
$addressLocality = get_field('addresslocality_json', 'options');
$addressRegion = get_field('addressregion_json', 'options');
$postalCode = get_field('postalcode_json', 'options');
$addressCountry = get_field('addresscountry_json', 'options');

$foundingDate = get_field('foundingdate_json', 'options');
$legalName = get_field('legalname_json', 'options');
// Fix Get Field with json Encode json_encode(field code inside)
//$page_url_json = get_the_permalink($post->ID);
//"@id" : "'. $page_url_json .'"
$html = '<script type="application/ld+json">
{ "@context": "http://schema.org",
"@type": "LegalService",
"legalName" : '. json_encode($legalName) .',
"foundingDate":  '. json_encode($foundingDate) .',
"founders": ['. $founders .'],
"contactPoint": ['. $contactPoint .'],
    "sameAs" : ['. $sameAs .'],
	"description": "'. $description .'",
	"telephone": '. json_encode(get_field('mainphone_json', 'options')) .',
	"image": "'. $image .'",
	"email" : "'. $email .'",
	"name":  '. json_encode($jname) .',
	"url": "'. get_home_url() .'",
	"logo": "'. get_field('logo_json', 'options') .'",
	"address": {
    "@type": "PostalAddress",
    "streetAddress": '. json_encode($streetAddress) .',
    "addressLocality": '. json_encode($addressLocality) .',
    "addressRegion": '. json_encode($addressRegion) .',
    "postalCode": '. json_encode($postalCode) .',
    "addressCountry": '. json_encode($addressCountry) .'
  },
  "openingHours": "'. $openingHours .'",
  "priceRange": "'. $range .'"
  }
  </script>';

	echo $html;

}
