<?php

/* TODO: Check for ACF version and ACF add-ons available */
/* Add ACF addons and ACF fields */
require_once('acf/init.php');

/* Include schema files */
require_once('inc/article.php');
require_once('inc/blog.php');
require_once('inc/news.php');
//require_once('inc/single-faq.php');
require_once('inc/faq-archive.php');
require_once('inc/images.php');
require_once('inc/legal-service.php');
require_once('inc/local-business.php');
require_once('inc/organization.php');
require_once('inc/person.php');
require_once('inc/service.php');
require_once('inc/single-testimonial.php');
require_once('inc/testimonials-archive.php');
require_once('inc/webpage.php');
require_once('inc/defined-term.php');
require_once('inc/defined-term-set.php');
require_once('inc/navigation.php');
/* Include Shortsodes */
require_once('shortcodes/video.php'); //Video Schema

/* Init schemas visible on the whole site */
add_action('wp_head', 'jsonldOrg'); //Organization Schema
add_action('wp_head', 'jsonldWebpage'); //Web Page Schema
add_action('wp_head', 'jsonldNav'); //Navigation Schema
jsonldImages(); //Images Schema

/* Conditional display Schemas */

add_action('wp_head', 'jsonldBlog'); //Blog Schema
add_action('wp_head', 'jsonldArticle'); //Article Schema
add_action('wp_head', 'jsonldNews'); //News Schema
add_action('wp_head', 'jsonldLocalBusiness'); //Local Business Schema
add_action('wp_head', 'jsonldLegalService'); //Organization LocalBusiness LegalService
//add_action('wp_head', 'jsonldSingleFAQ'); //Single FAQ Schema
add_action('wp_head', 'jsonldFAQarchive'); //Archive FAQ Schema
add_action('wp_head', 'jsonldSingleTestimonial'); //Single Testimonial Schema
add_action('wp_head', 'jsonldTestimonialsArchive'); // Testimonial Archive Schema
add_action('wp_head', 'jsonldService'); //Single Service Schema
add_action('wp_head', 'jsonldPerson'); //Single Service Schema
add_action('wp_head', 'jsonldDefinedTerm'); //DefinedTerm Schema
add_action('wp_head', 'jsonldDefinedTermSet'); //DefinedTerm Schema


// Strip Sortcodes in the Content
function hc_strip_shortcodes( $description ) {
    $pattern = get_shortcode_regex();
	return preg_replace('/'. $pattern .'/s', '', $description);
}

//Remove Yoast WebSite JSON LD
add_filter( 'disable_wpseo_json_ld_search', '__return_true' );
function hc_remove_yoast_json($data){
	if ($data['@type'] == 'WebSite') {
		$data = array();
	}
	return $data;
}
add_filter('wpseo_json_ld_output', 'hc_remove_yoast_json', 10, 1);
