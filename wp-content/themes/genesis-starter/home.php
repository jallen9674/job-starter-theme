<?php
/**
 * Blog Page Template
 */


//Adjust Body Classes
add_filter( 'body_class', 'hennessey_add_body_class' );
function hennessey_add_body_class( $classes ) {
	$classes[] = 'blog-listing';
	return $classes;
}

// Removes Skip Links.
remove_action( 'genesis_before_header', 'genesis_skip_links', 5 );

//Add back post titles for this page only
add_action( 'genesis_entry_header', 'genesis_do_post_title' );

//Filter Post Meta Information
add_filter( 'genesis_post_info', 'hennessey_post_meta' );
function hennessey_post_meta( $post_info ) {
	$post_info = 'Posted on [post_date]';
	return $post_info;
}

//Filter Main Page Title
remove_action( 'genesis_archive_title_descriptions', 'genesis_do_archive_headings_headline', 10, 3 );
add_action( 'genesis_archive_title_descriptions', 'hennessey_filter_archive_title', 10, 3 );

function hennessey_filter_archive_title( $heading = '', $intro_text = '', $context = '' ) {
  return null;
}

//Add Blog Heading Section
add_action('genesis_before_content', 'hennessey_blog_header');

// Runs the Genesis loop.
genesis();


