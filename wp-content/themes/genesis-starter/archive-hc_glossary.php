<?php
/**
 * Glossary Archive Template
 */

add_filter( 'body_class', 'hc_update_body_class' );
function hc_update_body_class( $classes ) {
	$classes[] = 'glossary-archive';
	return $classes;
}

//Remove Default Header Information
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

//Remove Genesis Generated Archives
remove_action( 'genesis_loop', 'genesis_do_loop' );

//Remove Default Title
add_filter( 'genesis_post_title_output', '__return_false' );

//Add Our Glossary Loop
add_action('genesis_loop', 'hennessey_glossary_archive', 5);


// Runs the Genesis loop.
genesis();

/*----------------------------------
Glossary Archive Loop
----------------------------------*/

function hennessey_glossary_archive(){

	$noPosts = true;
	$custom_terms = get_terms('hc_glossary_letter');

	foreach($custom_terms as $custom_term) {

		wp_reset_query();

		$args = array(
			'post_type' => 'hc_glossary',
			'posts_per_page' => -1,
			'orderby' =>'title',
			'order'=>'asc',
			'tax_query' => array(
				array(
					'taxonomy' => 'hc_glossary_letter',
					'field' => 'slug',
					'terms' => $custom_term->slug,
				),
			),
			);

			$loop = new WP_Query($args);
			if($loop->have_posts()) {

			//Flip noPosts variable to disable, no posts found text
			$noPosts = false;

			//Display Category Name
			echo '<span class="glossary-letter-heading">'.$custom_term->name.'</span>';

			echo '<div class="category-wrap"><ul class="glossary-list-terms">';
			//Display Resources Belonging to Category
			while($loop->have_posts()) : $loop->the_post();?>

				<li>
					<a href="<?php the_permalink(); ?>">
					<?php
					//Get Short Title If Set
						if ( get_post_meta( $post->ID, '_bg_glossary_alt_title', true ) ) {
							echo get_post_meta( $post->ID, '_bg_glossary_alt_title', true );
					} else {
						//If No Short Title Display Full Title
						the_title();
					}
					?>
					</a>
				</li>

			<?php endwhile;
			echo '</ul></div>';
			}
	}

	if($noPosts == true){echo 'No posts found.';}
	
	?>


</ul>

  <?php
}
