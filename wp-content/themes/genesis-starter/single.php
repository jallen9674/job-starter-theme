<?php
/**
 * Single Template
 */


//Filter Post Meta Information
add_filter( 'genesis_post_info', 'hennessey_post_meta' );

function hennessey_post_meta( $post_info ) {
	$post_info = 'Posted on [post_date]';
	return $post_info;
}

// Runs the Genesis loop.
genesis();

