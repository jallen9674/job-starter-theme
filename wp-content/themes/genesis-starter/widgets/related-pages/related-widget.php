<?php

// Creating the widget
class hc_related_widget extends WP_Widget {

    function __construct() {
    parent::__construct(

    // Base ID
    'hc_related_widget',

    // Widget
    'HC Related Widget',

    // Widget description
    array( 'description' => 'A widget that will display related practice area pages.')
    );
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );


    /* Get Current Post ID (For Grabbing Which Location to Display) */
    global $post;
    $locationCurrentPost = $post->ID;

    /*
    Get Current Location Taxonomy On Page
    */
    $postTerms =  wp_get_object_terms($post->ID, 'hc_related');
    $categoryFilterSlug = '';
    $categoryPrettyName = '';
    if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
         foreach ( $postTerms as $term ) {
           $categoryFilterSlug .= '' . $term->slug;
           $categoryPrettyName .= ' ' . $term->name;
         }
     }

     
     //Widget Toggle
    if($postTerms) :

    echo $args['before_widget'];
    // before and after widget arguments are defined by themes
    
    if ( ! empty( $title ) )

    //echo $args['before_title'] . $title . $args['after_title'];

    //BEGIN FRONTEND OUTPUT

        $nowtit = get_post_meta( $post->ID, '_hc_related_widget_title', true );


                $args = array(
                  'posts_per_page' => -1,
                  'post_type' => 'page',
                  'tax_query' => array(
                        array(
                            'taxonomy' => 'hc_related',
                            'field' => 'slug',
                            'terms' => $categoryFilterSlug
                        ),
                    ),
                   'meta_key'   => '_hc_related_widget_title',
                    'orderby'    => 'meta_value',
                    'order'      => 'ASC',
                  //'post__not_in' => array($locationCurrentPost),
                  // 'post_parent' => $post->post_parent
                );

                 
                $args['post_parent'] = $post->ID;

                $curtit = get_post_meta( $post->ID, '_hc_related_widget_title', true );
                
                if(!count(get_posts($args))) {
                    $args['post_parent'] = $post->post_parent;

                    $curtit = get_post_meta( $post->post_parent, '_hc_related_widget_title', true );
                }

                if(!count(get_posts($args))) {
                    unset($args['post_parent']);
                    $curtit = "";
                }


    ?>


    <div class="location-widget-outer">
        <div class="location-widget-inner">

            <div class="location-widget-title">
                <h3 class="location-widget-title__line-1"><?php echo $categoryPrettyName; ?></h3>
                <h3 class="location-widget-title__line-2">Practice Areas</h3>
            </div>

            <ul class="location-widget-links location-listing">

    
            <?php
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
           
                //Resetting Link Title For Errors
                $linkTitle = 'Not Set';

                //Updating Title For Current Link
                if ( get_post_meta( $post->ID, '_hc_related_widget_title', true ) ){
                        $linkTitle = get_post_meta( $post->ID, '_hc_related_widget_title', true );
                    }
            ?>
            <li class="single-location-link">
                <a href="<?php the_permalink(); ?>"><?php echo $linkTitle; ?></a>
            </li>
            
            <?php endwhile;  endif; ?>
            <?php wp_reset_query(); ?>

            <?php 
                //Adding Generic Pages to Child/GrandChild

                $parentPageTitle = get_the_title(wp_get_post_parent_id($post->ID));
                $grandparentPageTitle = get_the_title(wp_get_post_parent_id(wp_get_post_parent_id($post->ID)));
                
               if ( (strpos($grandparentPageTitle, 'Personal Injury') !== false) ) {
                    $args['post_parent'] = wp_get_post_parent_id($post->post_parent);

                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    ?>
                    <?php 
                        //Resetting Link Title For Errors
                        $linkTitle = 'Not Set';
    
                        //Updating Title For Current Link
                        if ( get_post_meta( $post->ID, '_hc_related_widget_title', true ) ){
                                $linkTitle = get_post_meta( $post->ID, '_hc_related_widget_title', true );
                            }
                    ?>
                    <li class="single-location-link secondary-location-link">
                        <a href="<?php the_permalink(); ?>"><?php echo $linkTitle; ?></a>
                    </li>
                
                <?php endwhile;  endif;  wp_reset_query();
                }

                $currentTitle = get_the_title($post -> ID );
                
                //This Should Be A List of All the Mid Level Pages with Sub-Pages

                if (    (strpos($currentTitle, 'Car Accident') !== false) ||
                        (strpos($currentTitle, 'Slip') !== false) ||
                        (strpos($currentTitle, 'Truck Accident') !== false) ||
                        (strpos($currentTitle, 'Motorcycle Accident') !== false) ||
                        (strpos($currentTitle, 'Work Injury') !== false) ||
                        (strpos($currentTitle, 'Construction') !== false) ||
                        (strpos($currentTitle, 'Medical Malpractice') !== false) ||
                        (strpos($currentTitle, 'Birth Injury') !== false) ||
                        (strpos($currentTitle, 'Tire Accident') !== false) 
                ) {
                    
                    $args['post_parent'] = $post->post_parent;
                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                    ?>
                    <?php 
                        //Resetting Link Title For Errors
                        $linkTitle = 'Not Set';
    
                        //Updating Title For Current Link
                        if ( get_post_meta( $post->ID, '_hc_related_widget_title', true ) ){
                                $linkTitle = get_post_meta( $post->ID, '_hc_related_widget_title', true );
                            }
                    ?>
                    <li class="single-location-link secondary-location-link">
                        <a href="<?php the_permalink(); ?>"><?php echo $linkTitle; ?></a>
                    </li>
                
                <?php endwhile;  endif;  wp_reset_query();

                }
                
               
                    
                   
                
            ?>


            </ul>
            
        </div>
    </div>

 </section>

    <?php
    //END FRONTEND OUTPUT

    echo $args['after_widget'];

     endif;
}

// Widget Backend
public function form( $instance ) {
   ?>
     <p>
      <em>No options available.</em>
     </p>
    <?php
}

// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
    }
} // Class wpb_widget ends here

// Register and load the widget
function hc_load_location_widget() {
    register_widget( 'hc_related_widget' );
}

add_action( 'widgets_init', 'hc_load_location_widget' );

