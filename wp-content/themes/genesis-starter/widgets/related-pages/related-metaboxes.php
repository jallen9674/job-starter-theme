<?php
/***************************************************
** ADDING METABOXES FOR LOCATION WIDGET
***************************************************/

add_action( 'cmb2_init', 'hc_related_group_meta' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function hc_related_group_meta() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_hc_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $hc_related_group_meta = new_cmb2_box( array(
        'id'            => 'hc_related_group_meta',
        'title'         => __( 'Related Page Group Information (Optional)', 'cmb2' ),
        'object_types'  => array( 'page'), // Post type
        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );

    $hc_related_group_meta->add_field( array(
        'name'     => __( 'Title', 'cmb2' ),
        'desc'     => __( 'Please enter the title that will be displayed in the related sidebar widget.', 'cmb2' ),
        'id'       => $prefix . 'related_widget_title',
        'type'     => 'text_medium', //taxonomy_select
        'taxonomy' => 'class_action_cat', // Taxonomy Slug
    ) );

    $hc_related_group_meta->add_field( array(
        'name'             => __( 'Top Level Page', 'cmb2' ),
        'desc'             => __( 'Set to true if this page should appear in the top level random listings.', 'cmb2' ),
        'id'               => $prefix . 'related_widget_top',
        'type'             => 'radio_inline',
        'show_option_none' => 'False',
        'options'          => array(
            'true' => __( 'True', 'cmb2' )
        ),
    ) );
}