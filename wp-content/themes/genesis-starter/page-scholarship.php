<?php
/**
 * Template Name: Scholarship Application
 */

add_filter( 'body_class', 'hennessey_add_body_class' );
function hennessey_add_body_class( $classes ) {
	$classes[] = 'scholarship-application fullwidth-template';
	return $classes;
}

//Remove Default the_content()
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

// Removes Skip Links.
remove_action( 'genesis_before_header', 'genesis_skip_links', 5 );

//Full Width Layou
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Adding Results Listing
add_action( 'genesis_before_content',  'hennessey_scholarship_layout');


// Runs the Genesis loop.
genesis();



//FAQ Loop

function hennessey_scholarship_layout(){
	?>
	
  <div class="scholarship-content">

      <main class="scholarship-content__left content">
        <?php echo the_content(); ?>
      </main>

      <aside class="scholarship-content__right">
        <?php echo do_shortcode('[contact-form-7 id="1710" title="Scholarship Application Form" html_class="scholarship-application-form"]'); ?>
      </aside>

  </div>

	<?php 
}
